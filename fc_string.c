/*
** fc_string.c for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Tue May 12 12:44:00 2015 Arthur Grosso
** Last update Thu May 14 14:06:00 2015 Arthur Grosso
*/

#include "tmt.h"
#include <stdlib.h>
#include <stdio.h>

void	show_tab(char **tab)
{
  int	i;

  i = 0;
  while (tab[i] != NULL)
    {
      printf("[%d] %s\n", i, tab[i]);
      i++;
    }
}

int	len_tab(char **tab)
{
  int	len;

  len = 0;
  while (tab[len] != NULL)
    {
      len++;
    }
  return (len);
}

char	**realloc_str_tab(char **tab)
{
  char	**new_tab;
  int	len;
  int	i;

  i = 0;
  if (tab == NULL)
    {
      new_tab = malloc(sizeof(char *) * 2);
      if (new_tab == NULL)
	return (NULL);
      new_tab[0] = NULL;
      new_tab[1] = NULL;
      return (new_tab);
    }
  len = len_tab(tab);
  new_tab = malloc((sizeof(char *) * (len + 2)) + 10);
  if (new_tab == NULL)
    return (NULL);
  while (i <= len)
    {
      new_tab[i] = tab[i];
      i++;
    }
  new_tab[i] = NULL;
  return (new_tab);
}

int	str_len(char *str)
{
  int	i;

  i = 0;
  if (str == NULL)
    return (0);
  while (str[i] != '\0')
    {
      i++;
    }
  return (i);
}

char	*str_dup(char *str)
{
  char	*out;
  int	i;

  i = 0;
  printf("dup {%s} %d\n", str, str_len(str));
  out = malloc(sizeof(char) * (str_len(str) + 1));
  if (out == NULL)
    return (NULL);
  while (i < (str_len(str)))
    {
      out[i] = str[i];
      i++;
    }
  out[i] = '\0';
  return (out);
}

char	*str_cat(char *str1, char *str2)
{
  int	len_str1;
  int	len_str2;
  char	*out;
  int	i;
  int	j;

  i = 0;
  j = 0;
  len_str1 = str_len(str1);
  len_str2 = str_len(str2);
  out = malloc(sizeof(char) * ((len_str1 + 1) + (len_str2 + 1) + 1));
  if (out == NULL)
    return (NULL);
  while (str1[i] != '\0')
    {
      out[j++] = str1[i++];
    }
  i = 0;
  while (str2[i] != '\0')
    {
      out[j++] = str2[i++];
    }
  out[j] = '\0';
  return (out);
}
