/*
** tmt.h for  in /home/grosso_a/workspace/tools/terminatest/include
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Tue May 12 00:40:20 2015 Arthur Grosso
** Last update Thu May 14 14:06:30 2015 Arthur Grosso
*/

#ifndef TMT_H_
# define TMT_H_

# define TO_READ 1024

# include <stdlib.h>
# include <unistd.h>
# include <string.h>
# include <stdio.h>

typedef struct	s_cmd
{
  char		*name;
  char		**cmd;
  char		**out;
  struct s_cmd	*next;
}		t_cmd;

typedef struct	s_tmt
{
  char		**file;
  char		**env;
  struct s_cmd	*cmd_list;
}		t_tmt;

char	*str_cat(char *str1, char *str2);
int	str_len(char *str);
char	*str_dup(char *str);
int	cmp_result(char **out, char *file_res);
int	is_file(char *name, char *ext);
int	pars_conf(t_tmt *tmt, char *file);
int	pars_cmd(t_tmt *tmt);
int	put_error(char *);
char	*get_next_line(const int fd);
int	len_tab(char **tab);
char	**realloc_str_tab(char **tab);
void	show_tab(char **tab);
int	exec_test(t_tmt *tmt);

#endif
