/*
** error.h for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Wed May 13 16:58:06 2015 Arthur Grosso
** Last update Thu May 14 16:50:25 2015 Arthur Grosso
*/

#ifndef ERROR_H_
# define ERROR_H_

/* Erreur */
# define NO_FILE	("FILE NOT EXISTE\n")
# define ERR_CMD	("Echec Commande\n")
# define CREAT_REP	("Création du répertoire")
# define REP_RESULT	("./test/result")
# define ERR_CREAT_REP	("Erreur création répertoire")
# define ERR_REGCOMP	("Error regcomp !\n")
# define INVALID_FILE	("\tInvalide file\n")

/* Parsing */
# define FILE_ID	("[FILE]")
# define ENV_ID		("[ENV]")
# define NAME_ID	("[NAME]")
# define CMD_ID		("[CMD]")
# define OUT_ID		("[OUT]")
# define SEP_ID		("--")
# define COMMENT	("@")
# define INIT_T		("\tDébut de la série de Test\n")
# define MASK		("#")
# define CONF_FILE	(".conf")
# define CMD_FILE	(".tmt")
# define RESULT_FILE	(".res")

/* Affichage */
# define CLEAR		("\033[cm")
# define NORMAL		("\033[0m")
# define RED		("\033[31m")
# define GREEN		("\033[32m")
# define YELLOW		("\033[33m")
# define BLUE		("\033[34m")
# define MAGENTA	("\033[35m")
# define CYAN		("\033[36m")
# define WHITE		("\033[37m")
# define MATCH		("Match !!")
# define NMATCH		("NO MATCH !!")
# define SEP_CMP	(" ?=? ")
# define SEP_RES_CMP	(" -> ")
# define CMD		("Commande ")
# define EXECUTE	("\tExecution ")
# define CMP		("\tComparaison")
# define TEST_DONE	("\tTest Done ")
# define SEP_CMD	("------------\n")
# define CREAT_RESULT	("Création du répertoire ")

#endif
