##
## Makefile for  in /home/grosso_a/workspace/wolf3d
##
## Made by Arthur Grosso
## Login   <grosso_a@epitech.net>
##
## Started on  Tue Dec  9 14:47:53 2014 Arthur Grosso
## Last update Fri May 15 04:23:12 2015 alexandre bonnetain
##

CC	= gcc

RM	= rm -rf

NAME	= termina-test

SRCS	= tmt.c \
	fc_string.c \
	cmp_result.c \
	get_next_line.c \
	exec.c \
	parseur.c

OBJS	= $(SRCS:.c=.o)

CFLAGS	+= -Wall -Wextra -g3
CFLAGS  += -I./include/

all:	 $(NAME)

$(NAME): $(OBJS)
	 $(CC) $(OBJS) -o $(NAME) $(CFLAGS)

clean:
	 -$(RM) $(OBJS)

fclean:  clean
	 -$(RM) $(NAME)

re:      fclean all

.PHONY: all clean fclean re
