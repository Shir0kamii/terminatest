/*
** tmt.c for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Tue May 12 00:10:15 2015 Arthur Grosso
** Last update Thu May 14 17:59:37 2015 Arthur Grosso
*/

#include "error.h"
#include "tmt.h"

int	put_error(char *str)
{
  (void)write (2, str, strlen(str));
  return (-1);
}

static void	set_env(char *cmd)
{
  int		len_key;

  len_key = 0;
  while (cmd[len_key] != '=' && cmd[len_key] != '\0')
    len_key++;
  cmd[len_key] = '\0';
  setenv(cmd, &(cmd[len_key+ 1]), 1);
}

static void	apply_env(t_tmt	*tmt)
{
  int	i;

  i = 0;
  printf("%s%s%s\n", BLUE, ENV_ID, NORMAL);
  while (tmt->env[i] != NULL)
    {
      printf("%s\n", tmt->env[i]);
      set_env(tmt->env[i]);
      i++;
    }
}

static int	get_all_conf(t_tmt *tmt, char **argv)
{
  int	i;

  i = 1;
  tmt->file = NULL;
  tmt->env = NULL;
  tmt->cmd_list = NULL;
  while (argv[i] != NULL)
    {
      if (is_file(argv[i], CONF_FILE))
	pars_conf(tmt, argv[i]);
      else if (is_file(argv[i], CMD_FILE))
	{
	  tmt->file = realloc_str_tab(tmt->file);
	  tmt->file[len_tab(tmt->file)] = argv[i];
	}
      else
	printf("%s%s%s%s\n", RED, INVALID_FILE, argv[i], NORMAL);
      i++;
    }
  pars_cmd(tmt);
  return 0;
}

int	main(int argc, char **argv)
{
  t_tmt	tmt;

  if (argc > 1)
    {
      get_all_conf(&tmt, argv);
      apply_env(&tmt);
      exec_test(&tmt);
    }
  return 0;
}
