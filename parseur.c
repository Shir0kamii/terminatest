/*
** parseur.c for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Tue May 12 00:50:03 2015 Arthur Grosso
** Last update Thu May 14 15:50:53 2015 Arthur Grosso
*/

#include "error.h"
#include "tmt.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int	is_file(char *name, char *ext)
{
  int	len;
  int	len_ext;

  len = strlen(name);
  len_ext = strlen(ext);
  if (len > len_ext)
    if (!strcmp(&(name[len - len_ext]), ext))
      return (1);
  return (0);
}

t_cmd	*add_cmd(t_cmd *cmd_list)
{
  t_cmd	*n_cmd;

  n_cmd = malloc(sizeof(t_cmd));
  if (n_cmd == NULL)
    return (cmd_list);
  n_cmd->name = NULL;
  n_cmd->cmd = NULL;
  n_cmd->out = NULL;
  if (cmd_list == NULL)
      n_cmd->next = NULL;
  else
    n_cmd->next = cmd_list;
  return (n_cmd);
}

void	get_line(t_tmt *tmt, char *str, int mode)
{
  if ((strlen(str)) && (strncmp(str, SEP_ID, 1)) && (strncmp(str, COMMENT, 1)))
    {
      switch (mode)
	{
	case 1:
	  tmt->file = realloc_str_tab(tmt->file);
	  tmt->file[len_tab(tmt->file)] = str;
	  break;
	case 2:
	  tmt->env = realloc_str_tab(tmt->env);
	  tmt->env[len_tab(tmt->env)] = str;
	  break;
	case 3:
	  tmt->cmd_list = add_cmd(tmt->cmd_list);
	  tmt->cmd_list->name = str;
	  break;
	case 4:
	  tmt->cmd_list->cmd = realloc_str_tab(tmt->cmd_list->cmd);
	  tmt->cmd_list->cmd[len_tab(tmt->cmd_list->cmd)] = str;
	  break;
	case 5:
	  tmt->cmd_list->out = realloc_str_tab(tmt->cmd_list->out);
	  tmt->cmd_list->out[len_tab(tmt->cmd_list->out)] = str;
	  break;
	}
    }
}

int	pars_conf(t_tmt *tmt, char *file)
{
  int	fd;
  char	*str;
  int	mode;

  mode = 0;
  get_next_line(-1);
  if ((fd = open(file, O_RDONLY)) == -1)
      return (put_error(NO_FILE));
  while ((str = get_next_line(fd)) != NULL)
    {
      if (!strcmp(str, FILE_ID))
	mode = 1;
      else if (!strcmp(str, ENV_ID))
	mode = 2;
      else if (strlen(str))
	get_line(tmt, str, mode);
    }
  return (0);
}

int	pars_cmd(t_tmt *tmt)
{
  int	fd;
  char	*str;
  int	i;
  int	mode;

  i = 0;
  mode = 0;
   while (tmt->file[i] != NULL)
    {
      get_next_line(-1);
      if ((fd = open(tmt->file[i++], O_RDONLY)) == -1)
	return (put_error(INVALID_FILE));
      while ((str = get_next_line(fd)) != NULL)
	{
	  if (!strcmp(str, NAME_ID))
	    mode = 3;
	  else if (!strcmp(str, CMD_ID))
	    mode = 4;
	  else if (!strcmp(str, OUT_ID))
	    mode = 5;
	  else if (strlen(str))
	    get_line(tmt, str, mode);
	}
    }
 return 0;
}
