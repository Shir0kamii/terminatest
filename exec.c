/*
** exec.c for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Wed May 13 16:07:13 2015 Arthur Grosso
** Last update Thu May 14 16:48:37 2015 Arthur Grosso
*/

#include <stdlib.h>
#include <stdio.h>
#include "tmt.h"
#include "error.h"

char	*gen_file(char *file_name)
{
  char	*out;

  out = str_cat("", REP_RESULT);
  out = str_cat(out, "/");
  out = str_cat(out, file_name);
  out = str_cat(out, RESULT_FILE);
  return (out);
}

char	*exec_cmd(char *cmd, char *name)
{
  char	*cmd_line;

  cmd_line = str_cat(cmd, " >> ");
  cmd_line = str_cat(cmd_line, gen_file(name));
  printf("%s%s\n", CMD, cmd_line);
  if (system(cmd_line))
    {
      printf(ERR_CMD);
      return (NULL);
    }
  return (cmd_line);
}

int	exec_test(t_tmt *tmt)
{
  t_cmd	*tmp;
  int	i;


  tmp = tmt->cmd_list;
  printf("%s%s%s%s\n", BLUE, CREAT_RESULT, REP_RESULT, NORMAL);
  system(str_cat("mkdir -v ", REP_RESULT));
  printf("%s%s%s\n", RED, INIT_T, NORMAL);
  while (tmp != NULL)
    {
      i = 0;
      system(str_cat("rm -f ", gen_file(tmp->name)));
      printf("%s%s%s", YELLOW, SEP_CMD, NORMAL);
      printf("%s%s%s%s\n", CYAN, EXECUTE, tmp->name, NORMAL);
      while (tmp->cmd[i])
      	exec_cmd(tmp->cmd[i++], tmp->name);
      printf("%s%s%s\n", MAGENTA, CMP, NORMAL);
      cmp_result(tmp->out, gen_file(tmp->name));
      tmp = tmp->next;
      printf("%s%s%s\n", BLUE, TEST_DONE, NORMAL);
    }
  return (0);
}
