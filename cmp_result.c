/*
** cmp_result.c for  in /home/grosso_a/workspace/tools/terminatest
** 
** Made by Arthur Grosso
** Login   <grosso_a@epitech.net>
** 
** Started on  Wed May 13 18:07:04 2015 Arthur Grosso
** Last update Thu May 14 15:48:25 2015 Arthur Grosso
*/

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <regex.h>
#include <stdio.h>
#include "tmt.h"
#include "error.h"

void		is_match(int match)
{
  if (match == 0)
    printf("%s%s%s\n", GREEN, MATCH, NORMAL);
  else
    printf("%s%s%s\n", RED, NMATCH, NORMAL);
}

int		str_cmp_regex(char *out, char *str)
{
  regex_t	mask;
  int		match;
  int		error;

  if ((str != NULL) && (str_len(str)))
    {
      if ((out != NULL) && strncmp(out, MASK, strlen(MASK)) == 0)
	{
	  printf("%s%s%s%s", &(out[strlen(MASK)]), SEP_CMP, str, SEP_RES_CMP);
  	  error = regcomp(&mask, &(out[strlen(MASK)]),
			  REG_EXTENDED | REG_NOSUB | REG_ICASE | REG_NEWLINE);
  	  if (error != 0)
  	    return(put_error(ERR_REGCOMP));
	  match = regexec (&mask, str, 0, NULL, 0);
	  is_match(match);
	  regfree(&mask);
	}
      else if (out != NULL && str != NULL)
	{
	  printf("%s%s%s%s", out, SEP_CMP, str, SEP_RES_CMP);
	  match = strcmp(str, out);
	  is_match(match);
	}
    }
  return (0);
}

int		cmp_result(char **out, char *file_res)
{
  char		*tmp;
  int		fd;
  int		i;

  i = 0;
  if ((fd = open(file_res, O_RDONLY)) == -1)
    return(put_error(INVALID_FILE));
  (void)get_next_line(-1);
  while ((tmp = get_next_line(fd)) != NULL)
    {
      str_cmp_regex(out[i], tmp);
      if (out[i + 1] != NULL)
	i++;
      free(tmp);
    }
  return (0);
}
