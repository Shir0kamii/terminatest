/*
** get_next_line.c for  in /home/grosso_a/workspace
**
** Made by grosso_a
** Login   <grosso_a@epitech.net>
**
** Started on  Tue Jan 13 16:40:15 2015 grosso_a
** Last update Tue May 12 11:56:45 2015 Arthur Grosso
*/

#include "tmt.h"
#include <string.h>

char	*my_realloc(char *str, int size)
{
  int	i;
  char	*new_str;

  new_str = malloc(sizeof(char) * size);
  if (new_str == NULL)
    return (NULL);
  i = 0;
  while (str[i] != '\0')
    {
      new_str[i] = str[i];
      i++;
    }
  new_str[i] = 0;
  free(str);
  return (new_str);
}

int		set_line(char *buffer, char *line,
			 int *cur_pos_line)
{
  static int	i = 0;

  while (buffer[i] != 0 && buffer[i] != '\n')
    {
      line[*cur_pos_line] = buffer[i];
      (*cur_pos_line)++;
      i++;
    }
  if (buffer[i] && buffer[i] == '\n')
    {
      line[*cur_pos_line] = 0;
      *cur_pos_line = 0;
      i++;
      return (0);
    }
  line[*cur_pos_line] = 0;
  i = 0;
  return (1);
}

char *init_get(char *buffer, int *cur_pos_line, int *end, int fd)
{
  char *get_line;

  get_line = malloc(sizeof(char) * TO_READ + 1);
  if (*end || (get_line == NULL) || (TO_READ == 0))
    {
      free(get_line);
      (fd == -1) ? (*end = 0) : (0);
      buffer[0] = '\0';
      *cur_pos_line = 0;
      return (NULL);
    }
  return (get_line);
}

char		*get_next_line(const int fd)
{
  static char	buffer[TO_READ + 1];
  static int	cur_pos_line = 0;
  static int	end = 0;
  int		len;
  char		*get_line;

  if ((get_line = init_get(buffer, &cur_pos_line, &end, fd)) == NULL)
    return (NULL);
  while (set_line(buffer, get_line, &cur_pos_line) && !end)
    {
      len = read(fd, buffer, TO_READ);
      buffer[len] = '\0';
      get_line = my_realloc(get_line, sizeof(char) * strlen(get_line)
			    + TO_READ + 1);
      if ((get_line == NULL) || len < 0)
	return (NULL);
      end = (!len ? 1 : 0);
    }
  if (get_line[0] == '\n')
    return (NULL);
  return (get_line);
}
